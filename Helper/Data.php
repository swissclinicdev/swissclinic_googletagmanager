<?php

namespace Swissclinic\GoogleTagManager\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected const XML_PATH_SWISSCLINIC_GTM_CONFIG= 'swissclinic_google_tag_manager/google_tag_manager_config/enable';
    protected const XML_PATH_SWISSCLINIC_GTM_ID= 'swissclinic_google_tag_manager/google_tag_manager_config/google_tag_manager_id';

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function isEnabled($storeId = null)
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_SWISSCLINIC_GTM_CONFIG,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function getId($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SWISSCLINIC_GTM_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

}