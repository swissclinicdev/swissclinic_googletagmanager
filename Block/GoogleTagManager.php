<?php
	
	namespace Swissclinic\GoogleTagManager\Block;

	class GoogleTagManager extends \Magento\Framework\View\Element\Template {
	    /**
	     * @var Swissclinic\GoogleTagManager\Helper\Data
	     */
	    protected $_helper;

	    /**
	     * Stripe constructor.
	     * @param \Magento\Catalog\Block\Product\Context $context
	     * @param array $data
	     * @param \Swissclinic\GoogleTagManager\Helper\Data $helper
	     */
	    public function __construct(
	        \Magento\Framework\View\Element\Template\Context $context,
	        array $data = [],
	        \Swissclinic\GoogleTagManager\Helper\Data $helper
	    ){

	        $this->_helper = $helper;

	        parent::__construct(
            $context,
            $data);
	    }

	    public function getId() {

	    	$text = $this->_helper->getId();

	    	return $text;
	    }
	}